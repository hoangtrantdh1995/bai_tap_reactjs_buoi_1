import logo from './logo.svg';
import './App.css';
import ExLayout from './BaiTapLayoutComponet/ExLayout';

function App() {
  return (
    <div>
      <ExLayout />
    </div>
  );
}

export default App;
