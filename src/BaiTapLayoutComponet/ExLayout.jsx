import React, { Component } from 'react'
import Content from './Content'
import Footer from './Footer'
import Header from './Header'
import TopHeader from './TopHeader'


export default class ExLayout extends Component {
    render() {
        return (
            <div>
                <TopHeader />
                <Header />
                <Content />
                <Footer />
            </div>
        )
    }
}
