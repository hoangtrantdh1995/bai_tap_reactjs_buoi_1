import React, { Component } from 'react'

export default class Content extends Component {
    render() {
        return (
            <div>
                <section className="pt-4">
                    <div className="container px-lg-5">
                        {/* start item */}
                        <div className="row gx-lg-5">
                            <div className="col-lg-6 col-xxl-4 mb-5">
                                <div className="card bg-light border-0 h-100">
                                    <div className="card-body border border-secondary" id='card-body'>
                                        <img src="https://toigingiuvedep.vn/wp-content/uploads/2021/05/hinh-anh-avatar-doremon-cute.png" alt="" className='w-100' />
                                    </div>
                                    <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0 item-card-body">
                                        <h2 className="fs-4 fw-bold">Card title</h2>
                                        <p className="mb-0">With text Bootstrap 5, we've created a fresh new layout for this template!</p>
                                    </div>
                                    <button className='btn btn-primary item-btn'>Find Out More!</button>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xxl-4 mb-5">
                                <div className="card bg-light border-0 h-100">
                                    <div className="card-body border border-secondary" id='card-body'>
                                        <img src="http://store-images.s-microsoft.com/image/apps.31554.9007199266247085.556f298d-2ae6-4c7e-a5c7-b59d0b6b82c4.b50b1775-f583-46cf-a68f-4d4314060163" alt="" className='w-100' />
                                    </div>
                                    <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0 item-card-body">
                                        <h2 className="fs-4 fw-bold">Card title</h2>
                                        <p className="mb-0">With Bootstrap 5, we've created a fresh new layout for this template!</p>
                                    </div>
                                    <button className='btn btn-primary item-btn'>Find Out More!</button>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xxl-4 mb-5">
                                <div className="card bg-light border-0 h-100">
                                    <div className="card-body border border-secondary" id='card-body'>
                                        <img src="https://backupgioithieutruyen.files.wordpress.com/2016/09/54771-just_super_saiyan_3_goku_by_shinthedragonfighter-d5ketzh.png?w=484&h=640" alt="" className='w-100' />
                                    </div>
                                    <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0 item-card-body">
                                        <h2 className="fs-4 fw-bold">Card title</h2>
                                        <p className="mb-0">With Bootstrap 5, we've created a fresh new layout for this template!</p>
                                    </div>
                                    <button className='btn btn-primary item-btn'>Find Out More!</button>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xxl-4 mb-5">
                                <div className="card bg-light border-0 h-100">
                                    <div className="card-body border border-secondary" id='card-body'>
                                        <img src="https://lequangstore.com/wp-content/uploads/2022/06/Pokemon-Lets-Go-Pikachu-Nintendo-Switch.webp" alt="" className='w-100' />
                                    </div>
                                    <div className="card-body text-center p-4 p-lg-5 pt-0 pt-lg-0 item-card-body">
                                        <h2 className="fs-4 fw-bold">Card title</h2>
                                        <p className="mb-0">With Bootstrap 5, we've created a fresh new layout for this template!</p>
                                    </div>
                                    <button className='btn btn-primary item-btn'>Find Out More!</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        )
    }
}
